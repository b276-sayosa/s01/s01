# [SECTION]
# Comments in python are done using "ctrl + /" or # symbol


# [SECTION] Python Syntax
# Hello World in python

print("Hello World!")

# [SECTION] Indentation
# Where in other programming languages the indentation in code is for readability only, the indentaion in python isvery important
# In Python, indentation is used to indicate a block of code.
# Similar to JS, there is no need to end statements with semicolons
# [Section] Variables
# Variables are the container of data
# In Python, a viriable is declared by stating the variable name and assigning a value using the equality symbol.


# [SECTION] Naming Convention snake casing
# The terminology used for variable names is identifier
# all identifiers should begin with a letter(A to Z or a to z ), dollar sign or an underscore
# after the first character, identifier can have any combination of characters
# unlike JavaScript that uses the camel casing, python uses the snake case for variables as defined in the PEP.
# most importantly, identifiers are case sensitive
age = 35
middle_initial = "C"

# python allows assigning of values to multiple variables in one line

name1, name2, name3, name4 = "john","paul","george","ringo"

print (name4)

# [SECTION] Data Types
# Data types convey what kind of information a variables hlds. There are different data types and each has its own use

# In python, there are commonly used data types:
# 1. String (str) - for alphanumeric and symbols
full_name = "John Doe"
secret_code = "Pa$$word"
#2. Numbers(int, float, complex) - for integers, decimal and complex numbers
num_of_days = 365 # this is an integer
pi_approx = 3.1416 #This is a float
complex_num = 1 + 5j #This is a complex number, letter j represents the imaginary number

print(type(full_name))

# 3. Boolean(bool) - for truth values
# Boolean values in Python start with uppercase letters
isLearning = True
isDifficult = False

#[SECTION] Using Variables
#Just like in JS variables are used by simply calling the name of the identifier

print("My name is " + full_name)

# [SECTION] Terminal Outputs
# In python, printing in the terminal uses the print() function
# To use variables, concatenate (+ symbol) between strings can be used
# however, we cannot concatenate string to a number or to a different data type
# print("My age is "+ age)

#[SECTION] Typecasting
# here are some functions that can be used in type castings
# 1 int()- convert the value into an integer
print(int(3.75))
# 2 float()- converts the value into an integer Value
print(float(5))
#3. str - converts the value into string
print("My age is " + str(age))


#Another way to avoid type error in printing without the use of typecasting
#f-strings
print(f"hi my name is {full_name} and my age is {age}.")

# [ SECTION ] OPERATIONs
# Python has operator families that can be used to manipulate variables

# Arithmetic Operator - performs mathematical operations

print(1+10)
print(15-8)
print(18*9)
print(21/7)
print(18%4)
print(2**6)

# Assignment operators - used to assign to variables 
num1 = 4

num1 += 3
print(num1)

#Other operators -=, *=, /=, %= 

# Comparison Operators - used to compare values ()
# boolean

print(1 == "1")
# other operators, !=, >=, <= ,>,<

#Logical operators - used to combine conditional statements 
# Logical or Operator
print(True or False and False)
# Logical and Operator
print(True and False)
# Logical Not Operator
print(not False)